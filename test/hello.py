import sys

from PyQt5.QtWidgets import (QMainWindow, QWidget, QToolTip, 
    QPushButton, QApplication, QLabel,QDesktopWidget,QMessageBox,QGridLayout)
from PyQt5.QtGui import QFont  


class Calculator(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
        
    def initUI(self):
        
        grid = QGridLayout()
        self.setLayout(grid)
 
        names = ['Cls', 'Bck', '', 'Close',
                 '7', '8', '9', '/',
                '4', '5', '6', '*',
                 '1', '2', '3', '-',
                '0', '.', '=', '+']
        
        positions = [(i,j) for i in range(5) for j in range(4)]
        
        for position, name in zip(positions, names):
            
            if name == '':
                continue
            button = QPushButton(name)
            grid.addWidget(button, *position)
            
        self.move(300, 150)
        self.setWindowTitle('Calculator')
        self.show()

class GuiSettings(QMainWindow):
    
    def __init__(self):
        super(GuiSettings, self).__init__()
        self.initUI()
        
        
    def initUI(self):
        
        QToolTip.setFont(QFont('SansSerif', 20))
        
        self.setToolTip('This is a <b>QWidget</b> widget')
        self.center()
        self.dialog = Calculator() 
        label = QLabel("Silicon Biosystem ToolTest", self)
        label.setToolTip("This is a <b>QLabel</b> widget with Tooltip")
        label.resize(label.sizeHint())
        label.move(80, 50)

        btn = QPushButton('Close', self)
        btn.setToolTip('This is a <b>QPushButton</b> widget')
        #btn.clicked.connect(QApplication.instance().quit)
        #btn.clicked.connect(self.closeEvent)
        btn.clicked.connect(self.calcutator_application)
        
        btn.resize(btn.sizeHint())
        btn.move(750, 550)       

        self.setGeometry(300, 300, 950, 650)
        self.statusBar().showMessage('Ready')
        self.setWindowTitle('Silicon Biosystem ToolTest')   
        
        self.show()

    def center(self):
        
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def calcutator_application(self):
        self.dialog.show()
        
    def closeEvent(self, event):
        
        reply = QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QMessageBox.Yes | 
            QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()   



def main():
    
    app = QApplication(sys.argv)
    ex = GuiSettings()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
