#!/bin/bash

echo RUN "main.py" script

sudo docker run -it \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $(pwd)/test:/app \
    -e DISPLAY=$DISPLAY \
    -u qtuser \
    fadawar/docker-pyqt5 python3 /app/hello.py


#END of run.sh script
